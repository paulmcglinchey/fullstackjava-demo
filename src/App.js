import { useEffect, useState } from "react";

const App = (props) => {

  const [count, setCount] = useState(0);

  useEffect(() => {
    document.title = `You clicked the button ${count} times`;
  })

  return (
    <div>
      <h1>You clicked the button {count} times!</h1>
      <button onClick={() => setCount(count + 1)}>
        Click me!
      </button>
    </div>
  )
}

export default App;